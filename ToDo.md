* Go through the settings for configuration, decide where they should be configured and implement and document the setting
 * Problem: Options are set in many different places/levels - like
   pad, canvas, plotStore and individual plots. Still in the end it
   would be nice if the user can change all options on "top level" (in
   the classes he actually interacts with)
  * Possible option 1: use the options dict (as in treePlotter) also
   for the plotStore - this allows flexibility that every plot can
   overwrite defaults, but the user still can configure them at each
   step
  * Possible option 2: have a wrapper class that creates automatically
   the correct plot depending on the options the user gives - these
   options can then also overwrite any defaults
  * To decide: Will we only have global options available to the user (easy to implement, but naming hell), or do we want to decide based on the plot (hopefuly sane default, but options need to be implemented per plot), or something inbetween?
* Default value for argument debugText in Pad.__init__ is [] - is this intended? (probably fine)
* Make a script to shuffle example/test plots from different revisions
* Currently the ymin/ymax determination and setting probably only works for histograms - have a look how this behaves for TGraphs and how to deal with it
* Generally it would be cool to have some way of treating graphs and histograms consistently
* TChain support for HistProjector/ProcessProjector would be cool
* Improve logging and document it
* Alias support in MultiHistDrawer
* Need to restructure the procedure of getting min/max etc ... again
* Fix something in the efficiency plots - the "fix" for min/max gives lots of warnings