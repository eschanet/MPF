# unfortunately i have to do this here
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

# user shouldn't use ROOT5
from .commonHelpers.logger import logger
logger = logger.getChild(__name__)
if int(ROOT.gROOT.GetVersion().split(".")[0]) < 6:
    logger.warning("You are using ROOT version {}, ROOT version 6 is recommended".format(ROOT.gROOT.GetVersion()))

# import some classes into the global namespace
from .plot import Plot
from .dataMCRatioPlot import DataMCRatioPlot
from .bgContributionPlot import BGContributionPlot
from .significanceScanPlot import SignificanceScanPlot
from .signalRatioPlot import SignalRatioPlot
from .treePlotter import TreePlotter

from . import pyrootHelpers as PH
