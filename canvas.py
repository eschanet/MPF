import ROOT
from .pad import Pad
from . import pyrootHelpers as PH
from . import globalStyle as gst

class Canvas(ROOT.TCanvas):
    def __init__(self, splitting, xAxisLabelsOption=None):
        super(Canvas, self).__init__("c{}".format(next(PH.tempNames)), "histo",
                                     0, 0, gst.canvasWidth, gst.canvasHeight)
        self.pads = {}
        self.splitting = splitting
        self.xAxisLabelsOption = xAxisLabelsOption
        self.setup()

    def setup(self):
        if self.splitting is None:
            self.pads['main'] = Pad("p{}".format(next(PH.tempNames)), (0,0,1,1))
        elif self.splitting == 'ratio':
            self.mainsize = gst.mainPadSize2Pad
            self.bottomsize = 1 - self.mainsize
            self.pads['main'] = Pad("main", (0,self.bottomsize,1,1), removeXLabels=True)
            self.pads['main'].SetBottomMargin(gst.ratioPlotMainBottomMargin)
            self.pads['bottom'] = Pad("bottom",
                                      (0,0,1,self.bottomsize),
                                      xTitleOffset=gst.ratioXtitleOffset,
                                      yAxisNDivisions=gst.ratioPadNDivisions,
                                      gridy=gst.ratioPadGridy,
                                      setNoExponent=gst.bottomPadsNoExponent,
                                      yTitleScale=gst.yTitleScale2Pad)
            self.pads['bottom'].SetTopMargin(0)
            self.pads['bottom'].SetBottomMargin(gst.ratioBottomMargin)
        elif self.splitting == '3pads':
            self.mainsize = gst.mainPadSize3Pad
            self.bottomsize = 1 - self.mainsize
            self.pads['main'] = Pad("main", (0,self.bottomsize,1,1), removeXLabels=True,
                                    yTitleScale=gst.yTitleScale3Pad)
            self.pads['main'].SetBottomMargin(0)
            self.pads['bottom1'] = Pad("bottom1", (0,gst.bottomPadSize3Pad,1,self.bottomsize),
                                       removeXLabels=True,
                                       yAxisNDivisions=gst.ratioPadNDivisions,
                                       gridy=gst.ratioPadGridy,
                                       setNoExponent=gst.bottomPadsNoExponent,
                                       yTitleScale=gst.yTitleScale3Pad)
            self.pads['bottom1'].SetTopMargin(0)
            self.pads['bottom1'].SetBottomMargin(0)
            self.pads['bottom2'] = Pad("bottom2", (0,0,1,gst.bottomPadSize3Pad),
                                       xTitleOffset=gst.xTitleOffset3Pad,
                                       yAxisNDivisions=gst.ratioPadNDivisions,
                                       gridy=gst.thirdPadGridy,
                                       setNoExponent=gst.bottomPadsNoExponent,
                                       yTitleScale=gst.yTitleScale3Pad)
            self.pads['bottom2'].SetTopMargin(0)
            self.pads['bottom2'].SetBottomMargin(gst.ratioBottomMargin)
        else:
            raise ValueError("splitting {} not supported".format(self.splitting))
        self.pads["main"].SetTopMargin(gst.mainPadTopMargin/self.pads["main"].GetAbsHNDC())
        # for key in self.pads:
        #     self.pads[key].Draw()

    def drawPads(self, **kwargs):
        for key in self.pads:
            if self.xAxisLabelsOption is not None:
                self.pads[key].xAxisLabelsOption = self.xAxisLabelsOption
            self.pads[key].draw(**kwargs)

    def saveAs(self, path, **kwargs):
        """
        Save the canvas, takes care of drawing everything and calls the ROOT Print function

        :param promptDir: if save path does not exist, ask if it
                          should be created. If set False, the dir is created without
                          asking (default: True)
        :param noSave: if set True the canvas is only drawn, but not
                       saved and left open, so custom modifications can be made (default: False)

        """

        import os.path
        from .commonHelpers.pathHelpers import ensurePathExists
        ensurePathExists(os.path.dirname(path), ask=kwargs.pop("promptDir", True))

        noSave = kwargs.pop("noSave", False)

        self.drawPads(**kwargs)
        self.cd()
        self.path = path # ToDo: Check if path has valid extension

        if not noSave:
            self.SaveAs(self.path)
            self.Close()

        return self.path


if __name__ == '__main__':
    can = Canvas()
    print( can)
