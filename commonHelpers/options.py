from collections import namedtuple

def checkUpdateDict(optDict, **kwargs):
    """Returns a dict containing the options from optDict, updated
    with kwargs. Raises typeError if an option in kwargs doesn't exist
    in optDict"""
    opt = dict(optDict)
    for key, value in optDict.items():
        opt[key] = kwargs.pop(key, value)
    if kwargs:
        raise TypeError("Got unexpected kwargs: {} - Try {}".format(kwargs, optDict.keys()))
    return opt

def popUpdateDict(optDict, **kwargs):
    """Returns a dict containing the options from optDict, updated
    with kwargs and the remaining kwargs that don't exist in optDict"""
    opt = dict(optDict)
    for key, value in optDict.items():
        opt[key] = kwargs.pop(key, value)
    return opt, kwargs

def checkUpdateOpt(optDict, **kwargs):
    """Returns a namedtuple containing the options from optDict, updated
    with kwargs. Raises typeError if an option in kwargs doesn't exist
    in optDict"""
    Opt = namedtuple("Opt", optDict.keys())
    return Opt(**dict(optDict, **kwargs))

def setAttributes(obj, optDict, **kwargs):
    for key, value in optDict.items():
        setattr(obj, key, kwargs.pop(key, value))
    if kwargs:
        raise TypeError("Got unexpected kwargs: {} - Try {}".format(kwargs, dir(obj)))
