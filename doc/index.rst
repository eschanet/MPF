.. MPF documentation master file, created by
   sphinx-quickstart on Sat Jan 21 23:43:03 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MPF's documentation!
===============================

MPF is a plotting framework that is meant to simplify the creation of typical "ATLAS-style" plots using `pyROOT <https://root.cern.ch/pyroot>`_.
Setup instructions and the code itself can be obtained `here <https://gitlab.cern.ch/bschacht/MPF>`_.

Plotting from histograms
------------------------

Plots can currently be created either from ROOT histograms. For example::

  from MPF.plot import Plot

  p = Plot()
  p.registerHist(hist1, style="background", process="Bkg1")
  p.registerHist(hist2, style="background", process="Bkg2")
  p.registerHist(hist3, style="signal", process="Signal")
  p.registerHist(hist4, style="data", process="Data")
  p.SaveAs(outputFilename)

See :py:mod:`~MPF.plotStore` for details and examples.

Plotting from ntuples
---------------------

Plots can also be generated from flat ntuples (`ROOT TTree
<https://root.cern.ch/doc/master/classTTree.html>`_). There is an
extensive interface wrapping around `TTree::Draw
<https://root.cern.ch/doc/master/classTTree.html#ac4016b174665a086fe16695aad3356e2>`_
to ease the creation of multiple similiar histograms from different
ntuples. For example::

  from MPF.treePlotter import TreePlotter

  tp = TreePlotter()
  tp.addProcessTree("Bkg1", rootfile1, treename1, style="background")
  tp.addProcessTree("Bkg2", rootfile2, treename2, style="background")
  tp.addProcessTree("Signal", rootfile3, treename3, style="signal")
  tp.addProcessTree("Data", rootfile4, treename4, style="data")
  tp.plot(outputFilename, varexp=var, xmin=xmin, xmax=xmax, nbins=nbins)

See :py:mod:`~MPF.treePlotter` for details and examples.

Also, there is an option to create multiple histograms from a single TTree by looping the tree only once. For example::

  tp.registerPlot(outputFilename1, varexp=var1, xmin=xmin1, xmax=xmax1, nbins=nbins1)
  tp.registerPlot(outputFilename2, varexp=var2, xmin=xmin2, xmax=xmax2, nbins=nbins2)
  tp.plotAll()

Global options
--------------

Most options are directly passed to the Functions/Classes that create
the plots. A few, more global options can also be set via the :py:mod:`MPF.globalStyle` module.


Memoization
-----------

All histograms that are created from ntuples are cached across
multiple executions (using the `meme <https://gitlab.cern.ch/nihartma/meme>`_ module), so once created,
modifications to the plot (e.g. style) can be made without having to
loop the ntuples again. This is very useful, however currently meme is
not thread-safe - so in case you are about to run MPF in parrallel in
the same folder you should deactivate the meme module::

  from MPF import meme
  meme.setOptions(deactivated=True)

By default the cache will be stored in a folder :code:`_cache` in the directory your code is executed. This can be changed by::

  meme.setOptions(overrideCache=myCachePath)

To enforce re-execution of cached functions either delete the cache folder or use::

  meme.setOptions(refreshCache=True)

Verbosity/Logging
-----------------

MPF uses the `builtin logging from python
<https://docs.python.org/2/library/logging.html>`_. Both MPF and meme
loggers are preconfigured if no handlers exist. You can set the
logging levels by retrieving the loggers. For example to deactivate
the INFO messages::

  import logging
  logging.getLogger("MPF").setLevel(logging.WARNING)
  logging.getLogger("meme").setLevel(logging.WARNING)

The submodules of MPF use child loggers of the :code:`MPF` logger. The
naming scheme is :code:`MPF.MPF.<submodule-name>`. For example to
activate the DEBUG messages only for the :py:mod:`~MPF.histProjector`
module do::

  logging.getLogger("MPF.MPF.histProjector").setLevel(logging.DEBUG)

If you want to configure your own loggers you can do this by either
adding handlers to the MPF/meme or root logger prior to any import of
MPF modules. Alternatively you can (also prior to any import of MPF
modules) add a NullHandler to the MPF logger and configure logging at
any time later::

  logging.getLogger("MPF").addHandler(logging.NullHandler())


Table of contents
=================

.. toctree::
   MPF
   :maxdepth: 1
   :caption: Contents:




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
