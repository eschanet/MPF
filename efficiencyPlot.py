#!/usr/bin/env/python
"""
Plot the ratio of histograms where the numerator is assumed to be filled
with a subset of the events of the denominator.

The first registered histogram is the denominator, all further
histograms are treated as numerators. 

Example
--------

.. literalinclude:: /../examples/efficiencyPlot.py

.. image::  images/efficiencyPlot.png
   :width: 600px

"""

from .plotStore import PlotStore

class EfficiencyPlot(PlotStore):

    """

    :param doDataMCRatio: if both data and MC is added, also plot the data/MC ratio (default: False)

    Overwrites the defaults for the following :py:meth:`~MPF.plotStore.PlotStore` parameters:

    :param ratioMode: default: "binomial"
    :param drawRatioLine: default: False
    :param yTitle: default: #epsilon

    For further options see :py:meth:`~MPF.plotStore.PlotStore`
    """

    def __init__(self, **kwargs):
        self.doDataMCRatio = kwargs.pop("doDataMCRatio", False)
        if self.doDataMCRatio:
            splitting = "ratio"
        else:
            splitting = None
        super(EfficiencyPlot, self).__init__(ratioMode=kwargs.pop("ratioMode", "binomial"),
                                             drawRatioLine=kwargs.pop("drawRatioLine", False),
                                             yTitle=kwargs.pop("yTitle", "#epsilon"),
                                             splitting=splitting,
                                             **kwargs)

    def registerHist(self, hist, **kwargs):
        """
        Overwrites the defaults for the following :py:meth:`~MPF.plotStore.PlotStore.registerHist` parameters:

        :param hide: default: True
        :param style: default: "signal"

        For further options see :py:meth:`~MPF.plotStore.PlotStore.registerHist`
        """
        super(EfficiencyPlot, self).registerHist(hist,
                                                 hide=kwargs.pop("hide", True),
                                                 style=kwargs.pop("style", "signal"),
                                                 **kwargs)

    def saveAs(self, path, **kwargs):
        self.buildMainPad(**kwargs)

        self.addSignalRatios(self.canvas.pads['main'], addLegend=True, register=True, applyRatioLimits=False)
        if len(list(self.getHists("data"))) > 0:
            self.addSignalRatios(self.canvas.pads['main'], addLegend=True, register=True, style="data", applyRatioLimits=False)
            if self.doDataMCRatio:
                self.addDataMCRatio(self.canvas.pads['bottom'], ratioMode="pois", drawRatioLine=True)

        return super(EfficiencyPlot, self).saveAs(path, **kwargs)
