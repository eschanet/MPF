from . import plotFromTree
from . import plotFromHistogram

from .exampleHelpers import parseExampleArgs
args = parseExampleArgs()

plotFromTree.main(args)
plotFromHistogram.main(args)
