#include <iostream>
#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include <TGenPhaseSpace.h>
#include <TRandom.h>
#include <TString.h>
#include <vector>

class exampleTree {
public:
  void smear(TLorentzVector* v, float rel=0.1) {
    v->SetPtEtaPhiM((1+gRandom->Gaus()*rel)*v->Pt(),
                    (1+gRandom->Gaus()*rel)*v->Eta(),
                    (1+gRandom->Gaus()*rel)*v->Phi(),
                    v->M());
    return;
  }
  TTree* makeWTree(float mass, TString treeName, int nevents=100000, float lmass=0., float numass=0.) {
    /*
      This is the most sophisticated MC generator on earth
     */
    TTree* tree = new TTree(treeName, treeName);

    gRandom->SetSeed(1234);

    TGenPhaseSpace* event1 = new TGenPhaseSpace();
    TGenPhaseSpace* event2 = new TGenPhaseSpace();

    // Incoming protons
    float Ep = 6500.;
    TLorentzVector p1;
    TLorentzVector p2;
    p1.SetPxPyPzE(0., 0., Ep, Ep);
    p2.SetPxPyPzE(0., 0., -Ep, Ep);

    TLorentzVector initial;
    // Double_t decayMasses1[3] = {0., 0., mass};
    // Double_t decayMasses2[2] = {0., 0.};

    // W+jets (dynamically generated for each event)
    std::vector<Double_t> decayMasses1;

    // W-decay
    std::vector<Double_t> decayMasses2 {lmass, numass};

    std::vector<TLorentzVector*> jets;
    TLorentzVector* W;
    TLorentzVector* v1;
    TLorentzVector* v2;

    TLorentzVector v_met;
    TLorentzVector v_tmp;

    float m;
    float mt;
    float met;
    float ptl;
    float ptj;
    float ht;
    int njets;
    int nExtraJets;
    int nFilterJets = 2;
    float x1;
    float x2;
    float w;
    std::vector<float> jets_pt;
    tree->Branch("m", &m, "m/F");
    tree->Branch("mt", &mt, "mt/F");
    tree->Branch("met", &met, "met/F");
    tree->Branch("ptl", &ptl, "ptl/F");
    tree->Branch("ptj", &ptj, "ptj/F");
    tree->Branch("ht", &ht, "ht/F");
    tree->Branch("w", &w, "w/F");
    tree->Branch("njet", &njets, "njets/I");
    tree->Branch("jets_pt", &jets_pt);
    for (int i=0; i<nevents; i++) {

      jets_pt.clear();

      // PDF
      x1 = gRandom->Rndm()*0.3;
      x2 = gRandom->Rndm()*0.3;
      initial = x1*p1+x2*p2;

      // complex simulation of additional parton radiation
      nExtraJets = int(gRandom->Exp(0.5));
      njets = nExtraJets + nFilterJets;

      // simulate the hard process
      decayMasses1.clear();
      decayMasses1.push_back(mass); // here comes the W
      for (int i=0; i<njets; i++) {
        decayMasses1.push_back(0.);
      }
      event1->SetDecay(initial, decayMasses1.size(), decayMasses1.data());
      event1->Generate();

      W = event1->GetDecay(0);
      jets.clear();
      for (int i=1; i<njets+1; i++) { // go until njets+1 since we start at 1
        jets.push_back(event1->GetDecay(i));
      }

      // simulate W decay
      event2->SetDecay(*W, decayMasses2.size(), decayMasses2.data());
      event2->Generate();
      v1 = event2->GetDecay(0);
      v2 = event2->GetDecay(1);

      // complex detector and met simulation
      smear(v1, 0.1);
      smear(v2, 0.1);
      v_met.SetPxPyPzE(0, 0, 0, 0);
      ht = 0.;
      for (auto jet : jets) {
        smear(jet, 0.2);
        v_met -= *jet;
        ht += jet->Pt();
        jets_pt.push_back(jet->Pt());
      }
      v_met -= *v1;
      // v2 is invisible

      v_met.SetPxPyPzE(v_met.Px(), v_met.Py(), 0., v_met.Pt());

      // Calculate variables
      m = (*v1+*v2).M(); // just for fun v2 is not invisible here
      v_tmp.SetPxPyPzE(v1->Px(), v1->Py(), 0., v1->Pt());
      mt = (v_tmp+v_met).M();
      ptl = v1->Pt();
      ptj = jets[0]->Pt();
      met = v_met.Pt();

      // Random weight
      w = gRandom->Gaus(1., 0.05);

      tree->Fill();
    }
    return tree;
  }
};
