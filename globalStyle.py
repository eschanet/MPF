"""
Some fixed settings to make the plots look nice by default. You can
import this module and change some settings if you like - for Example::

    import MPF.globalStyle as gst

    gst.ratioErrorBandFillStyle = 3354
    gst.ratioErrorBandColor = ROOT.kBlack
    gst.drawATLASLabel = False

Alterantively you can also temporarily set options - e.g. for one plot
- by using the :py:class:`~MPF.globalStyle.useOptions` context manager::

    import MPF.globalStyle as gst
    from MPF.globalStyle import useOptions

    with useOptions(ratioErrorBandFillStyle=3354, ratioErrorBandColor=ROOT.kBlack):
        p.plot("output.pdf")

"""

import ROOT
import sys

class useOptions:
    """Context manager for temporarily setting options"""
    def __init__(self, optObject=sys.modules[__name__], **kwargs):
        self.optObject = optObject
        self.oldOpts = {}
        self.newOpts = kwargs

    def __enter__(self):
        for opt, value in self.newOpts.items():
            self.oldOpts[opt] = getattr(self.optObject, opt)
            setattr(self.optObject, opt, value)

    def __exit__(self, etype, value, traceback):
        for opt, value in self.oldOpts.items():
            setattr(self.optObject, opt, value)
        print(self.optObject.canvasWidth)


canvasWidth = 800
"""horizontal plot size - changing this might require to change also
some other options"""

canvasHeight = 600
"""vertical plot size - changing this might require to change also
some other options"""

mainPadSize2Pad = 0.7
"""Relative size of the mainpad for the splitting with 2 pads"""

mainPadSize3Pad = 0.5
"""Relative size of the mainpad + first bottom pad for the splitting with 3 pads"""

bottomPadSize3Pad = 0.3
"""Relative size of the second bottom pad for the splitting with 3 pads"""

ratioPadNDivisions = 504
"""Axis tick divisions on the ratio pads
n = n1 + 100*n2 + 10000*n3"""

ratioPadGridy = 1
"""draw vertical lines on the yAxis ticks of the ratio pad This is used
by default for the first bottom pad - assuming it will be some ratio
like graph in there (individual plots can overwrite this option)"""

thirdPadGridy = 1
"""draw vertical lines on the yAxis ticks of the third bottom pad
(individual plots can overwrite this option)"""

ratioBottomMargin = 0.4
"""bottom margin in ratio plots"""

ratioPlotMainBottomMargin = 0.04
"""Bottom Margin on the main pad in ratio plots"""

mainPadTopMargin = 0.06
"""Top margin in main pad (relative to absolute canvas height)"""

ratioXtitleOffset = 3
"""Ratio xTitle offset"""

xTitleOffset3Pad = 2.4
"""Ratio xTitle offset"""

yTitleOffset = 1.6
"""default yTitle offset for all pads"""

labelFont = 43
"""Font number for axis and labels (43 is fixed size, atlasStyle default is taken if set to None)"""

labelFontSize = 30
"""Font size for axis and labels (atlasStyle default taken if set to None)"""

yTitleScale2Pad = 0.9
"""Scale y-Axis Titles for 3 pad plots"""

yTitleScale3Pad = 0.8
"""Scale y-Axis Titles for 3 pad plots"""

legendYMax = 0.92
"""Legend default y position"""
legendXMin = 0.7
"""Legend default x position"""

legendTextSize = 0.04
"""Default Text size for legend (if None, text will be scaled to fit on legend size)"""

legendFont = 42
"""Legend Font"""

legendLongTitleThreshold = 11
"""Length starting from which a title in the legend is considered long and text will be scaled"""

legendBorderSize = 0
"""Useful to set this nonzero for debugging"""

totalBGErrorColor = ROOT.kBlack
"""Color for totalBG error bands"""

totalBGFillStyle = 3354
"""Fill style for totalBG error"""

ratioErrorBandColor = ROOT.kOrange-4
"""Color in ratio error bands"""

ratioErrorBandFillStyle = 1001
"""Fill style for ratio error bands"""

bottomPadsNoExponent = True
"""No Exponent on y-axis of bottom pads - this won't look nice if there
are large numbers involved (and no log scale is used), but the
exponent doesn't fit there"""

drawATLASLabel = True
"""Draw AtlasLabel in plots?"""

atlasLabelTextSize = 0.04 #: Text size for atlasLabel
lumiLabelTextSize = 18 #: Text size for lumiLabel 
CMELabelTextSize = lumiLabelTextSize #: Text size for CMELabel

atlasLabelDelX = 2.5*atlasLabelTextSize
"""Measure for distance between "ATLAS" and the text"""

atlasLabelX = 0.19 #: Default x position for atlasLabel
atlasLabelY = 0.88 #: Default x position for atlasLabel
lumiLabelX = 0.19 #: Default x position for lumiLabel
lumiLabelY = 0.83 #: Default x position for lumiLabel
CMELabelX = lumiLabelX+0.009*lumiLabelTextSize #: Default x position for CMELabel
CMELabelY = lumiLabelY #: Default x position for CMELabel

defaultLogyYmin = 0.1
"""Default y mininum on log scale plots
(in case not explicitely set or automatically determined)"""

maximumWithErrors = True #: Loop over bins to find maximum with errors for histograms
minimumWithErrors = False #: Loop over bins to find minimum with errors for histograms

TLineStyle = 2 #: Default TLine style
TLineWidth = 2 #: Default TLine width
TLineColor = 1 #: Default TLine color

legendEventCountFormat = " ({:.1g})"
"""`Format <https://docs.python.org/2/library/string.html#format-string-syntax>`_
for showing event counts in the legend (if set)"""

legendEventCountFormatRaw = " ({:.0g})"
"""`Format <https://docs.python.org/2/library/string.html#format-string-syntax>`_
for showing (raw) event counts in the legend (if set)"""

legendShowScaleFactors = True
"""Show scale factors of processes in legend?"""

customTextSize = 18
"""Size of custom text labels"""

customTextFont = 43
"""Font of custom text labels"""

thankYou = True #: Thank MPF at exit of your script
