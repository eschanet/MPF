import copy
import math

import ROOT

from .pyrootHelpers import tempNames, rootStyleColor
from . import pyrootHelpers as PH

from .commonHelpers.logger import logger
logger = logger.getChild(__name__)

class Graph(object):
    def __init__(self, **kwargs):
        self.color = 'kGray'

    def setColor(self):
        logger.debug('set color of {} to {}'.format(self, self.color))

        color = PH.rootStyleColor(self.color)

        self.SetLineColor(color)
        self.SetMarkerColor(color)
        self.SetMarkerStyle(6)
        self.SetLineWidth(2)
        self.SetFillColor(color)
        
    def draw(self, **kwargs):
        logger.debug("Drawing {}".format(self))
        self.setColor()
        self.Draw('p')
        

class Histogram(object):
    def __init__(self):

        self.process = ''
        self.style = None

        # default values
        self.color = 'kGray'
        self.lineStyle = None
        self.lineWidth = None
        self.legend = ''
        self.hide = False
        self.bins = self.GetSize()-2 # subtract over- and underflow bin
        self.binmin = self.GetXaxis().GetBinLowEdge(1)
        self.binmax = self.GetXaxis().GetBinUpEdge(self.bins)
        self.drawString = ''
        self.zIndex = 0
        self.maskedBins = []
        self.zoomBins = []
        self.unit = None
        self.fillStyle = None
        self.markerStyle = None

        self.xTitle = self.GetXaxis().GetTitle()
        self.yTitle = self.GetYaxis().GetTitle()

        # set to None if empty
        if self.xTitle == "":
            self.xTitle = None
        if self.yTitle == "":
            self.yTitle = None

    def __lt__(self, other):
        return self.Integral() < other.Integral()

    def add(self, other):
        self.Add(other)

    def rebin(self, rebin=1):
        self.bins = self.GetSize()-2
        if (self.bins)%rebin>0:
            logger.warning("trying to rebin {} with {} bins by a factor of {}".format(self.title, self.bins, rebin))
        else:
            self.bins = self.bins/rebin
        self.Rebin(rebin)

    def setColorAndStyle(self):
        logger.debug('set color of {} to {}'.format(self, self.color))

        color = rootStyleColor(self.color)

        if self.fillStyle is not None:
            self.SetFillStyle(self.fillStyle)

        if self.markerStyle is not None:
            self.SetMarkerStyle(self.markerStyle)

        if self.style == 'signal':
            self.SetLineColor(color)
            if self.lineStyle is None:
                self.SetLineStyle(2)
            else:
                self.SetLineStyle(self.lineStyle)
            if self.lineWidth is None:
                self.SetLineWidth(2)
            else:
                self.SetLineWidth(self.lineWidth)
            if self.fillStyle is not None:
                self.SetFillColor(color)
        elif self.style == 'data':
            self.SetMarkerColor(color)
            self.SetLineColor(color)
        else:
            self.SetFillColor(color)

    def getXTitle(self):
        if self.xTitle is None:
            return ""
        elif self.unit is not None:
            return "{} [{}]".format(self.xTitle, self.unit)
        else:
            return self.xTitle

    def getYTitle(self):
        if self.yTitle is not None:
            return self.yTitle
        elif not self.GetXaxis().IsVariableBinSize():
            if self.unit == 'GeV':
                return 'Entries / {:.0f} {}'.format((self.binmax-self.binmin)/self.bins, self.unit)
            elif self.unit is not None:
                return 'Entries / {} {}'.format((self.binmax-self.binmin)/self.bins, self.unit)
        return 'Entries'

    def underflow(self):
        return self.GetBinContent(0)
    def overflow(self):
        return self.GetBinContent(self.GetNbinsX()+1)

    def firstDraw(self, **kwargs):
        self.draw(**kwargs)

    def draw(self, drawString = ""):
        logger.debug("Drawing {}".format(self))
        drawString += self.drawString
        self.setColorAndStyle()
        if self.style == 'signal':
            # if not 'E' in drawstring:
            drawString += 'hist'
        elif self.style == 'data':
            drawString += 'PE0'
        elif self.style == 'axis':
            drawString = "axis"
        else:
            drawString += 'hist'
        logger.debug('drawing {} with options {} in color {} with Integral {}'.format(self.GetName(), drawString, self.color, self.Integral()))

        for i in self.maskedBins:
            self.SetBinContent(i,0)
            self.SetBinError(i,0)
        self.Draw(drawString)

    def truncateErrors(self, value=0):
        for i in range(1, self.GetNbinsX()+1):
            self.SetBinError(i, value*self.GetBinContent(i))

    def clone(self):
        """Clones a histogram.
        Until i find out how to do this properly
        (e.g. with deepcopy) do some stuff manually here
        """
        cloneHist = getHM(self.Clone(next(tempNames)))
        self.cloneAttributes(cloneHist, self)
        return cloneHist

    @staticmethod
    def cloneAttributes(tohist, fromhist):
        for attribute in ["color", "xTitle", "yTitle", "style", "zIndex", "lineWidth", "lineStyle", "unit"]:
            try:
                setattr(tohist, attribute, getattr(fromhist, attribute))
            except AttributeError as e:
                logger.warning("couldn't clone all expected attributes: {}".format(e))


    yieldBinNumbers = PH.yieldBinNumbers


    def addSystematicError(self, *hists, **kwargs):
        """
        Add systematic variations to the errorband based on given
        variational histogram(s).

        :param hists: one ore more histograms to be added
        :param mode: how to add and symmetrise the errors?

                     - symUpDown (default): independently add up and down variations quadratically and symmetrise afterwards
                     - largest: also add quadratically up and down variations, but then use the max(up, down) as the error

        """
        mode = kwargs.pop("mode", "symUpDown")
        if kwargs:
            raise KeyError("Got unexpected kwargs: {}".format(kwargs))
        skippedBinHists = set()
        for i in self.yieldBinNumbers(overFlow=True, underFlow=True):
            totalUp = 0.
            totalDown = 0.
            for hist in hists:
                if hist.GetBinContent(i) == 0:
                    try:
                        histName = hist.process
                    except AttributeError:
                        histName = hist.GetName()
                    logger.debug("Got a 0 bin content at bin {} for {}- ignoring it".format(i, histName))
                    skippedBinHists.add(histName)
                    continue
                delta = hist.GetBinContent(i)-self.GetBinContent(i)
                if delta > 0:
                    totalUp += delta**2
                else:
                    totalDown += delta**2
            totalUp = math.sqrt(totalUp)
            totalDown = math.sqrt(totalDown)
            oldError = self.GetBinError(i)
            if mode == "symUpDown":
                newError = 0.5*(totalUp+totalDown)
            elif mode == "largest":
                newError = max(totalUp, totalDown)
            else:
                raise ValueError("Unknown mode {}".format(mode))
            self.SetBinError(i, math.sqrt(oldError**2+newError**2))
        if skippedBinHists:
            logger.warn("Got 0 bin contents for one or more bins for {} - ignored them".format(",".join(skippedBinHists)))


    def addOverflowToLastBin(self):
        """
        Add histograms overflow bin content (and error) to the last bin
        """
        self = PH.addOverflowToLastBin(self)


class HistogramD(ROOT.TH1D, Histogram):
    def __init__(self, histogram, **kwargs):
        super(HistogramD, self).__init__(histogram)
        # ROOT.TH1D.__init__(self, histogram)
        Histogram.__init__(self, **kwargs)


class HistogramF(ROOT.TH1F, Histogram):
    def __init__(self, histogram, **kwargs):
        super(HistogramF, self).__init__(histogram)
        # ROOT.TH1F.__init__(self, histogram)
        Histogram.__init__(self, **kwargs)


class HistogramStack(ROOT.THStack):
    def __init__(self, histogram):
        super(HistogramStack, self).__init__(histogram)
        self.xTitle = None
        self.yTitle = None
        self.drawString = ''
        self.zIndex = 0
        self.bins = None
        self.binmin = None
        self.binmax = None
        self.unit = None

    def firstDraw(self, **kwargs):
        self.draw(**kwargs)

    def draw(self, drawString=''):
        logger.debug("Drawing {}".format(self))
        drawString = self.drawString + drawString
        self.Draw('hist{}'.format(drawString))

    def checkSet(self, attr, to):
        if getattr(self, attr) is None:
            setattr(self, attr, to)
        if not getattr(self, attr) == to:
            logger.warning("Trying to add a histogram with inconsistent attribute \"{}\" = {}"
                           "to the stack (stack value: \"{}\")".format(attr, to, getattr(self, attr)))

    def add(self, hist):
        self.checkSet("bins", hist.GetSize()-2)
        self.checkSet("binmin", hist.GetXaxis().GetBinLowEdge(1))
        self.checkSet("binmax", hist.GetXaxis().GetBinUpEdge(self.bins))
        if self.xTitle is None:
            self.checkSet("xTitle", hist.getXTitle())
        if self.yTitle is None:
            self.checkSet("yTitle", hist.getYTitle())
        super(HistogramStack, self).Add(hist)

    def Add(self, hist):
        logger.warn("You should not use HistogramStack.Add (capital a) - better create an"
                    "MPF histogram (getHM) and add it using HistogramStack.add (lowercase a)")
        super(HistogramStack, self).Add(hist)

    def getYTitle(self):
        return self.yTitle

    def getXTitle(self):
        return self.xTitle



class WrapTGraphAsymmErrors(ROOT.TGraphAsymmErrors, Graph):
    def __init__(self, graph, **kwargs):
        super(WrapTGraphAsymmErrors, self).__init__(graph)
        self.drawString = ''
        self.zIndex = 0
        Graph.__init__(self, **kwargs)

def getHM(histogram):
    # create extended histogram
    if histogram.ClassName()[:4] == 'TH1D':
        return HistogramD(histogram)
    elif histogram.ClassName()[:4] == 'TH1F':
        return HistogramF(histogram)
    elif histogram.ClassName() == 'THStack':
        return HistogramStack(histogram)
    elif histogram.ClassName() == 'TGraphAsymmErrors':
        return WrapTGraphAsymmErrors(histogram)
    else:
        logger.warning('{} not implemented'.format(histogram.ClassName()))
        return
