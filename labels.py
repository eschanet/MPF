import ROOT

from . import globalStyle as gst

def scaleYPosTopMargin(y):
    """Rescale the y position (NDC) in current pad to have the same distance to
    the top in different pad heights
    """
    return 1 - (1-y)/ROOT.gPad.GetAbsHNDC()

class ATLASLabel(object):

    "The ATLAS Label"

    def __init__(self, text="Internal", color=ROOT.kBlack, scale=1.0, xOffset=0., yOffset=0.):

        """

        :param text: Text next to "ATLAS"
        :param color: Text color
        :param scale: Scale overall text size by this factor
        :param xOffset: Shift by this amount in x-Direction
        :param yOffset: Shift by this amoutn in y-Direction

        """

        self.text = text
        self.color = color
        self.scale = scale
        self.xOffset = xOffset
        self.yOffset = yOffset
        self.zIndex = 2

    def draw(self, *args, **kwargs):

        if not gst.drawATLASLabel:
            return

        # Distance between "ATLAS" and the text
        self.delx = gst.atlasLabelDelX/ROOT.gPad.GetAbsWNDC()*self.scale

        self.x = gst.atlasLabelX
        self.y = gst.atlasLabelY

        # rescale to same distance to top
        self.y = scaleYPosTopMargin(self.y)

        # custom offset is absolute, so won't be the same for
        # different pad heights
        self.y += self.yOffset
        self.x += self.xOffset

        textSize = self.scale*gst.atlasLabelTextSize/ROOT.gPad.GetAbsHNDC()

        l = ROOT.TLatex()
        l.SetNDC()
        l.SetTextFont(71)
        l.SetTextSize(textSize)
        l.SetTextColor(self.color)
        self.l = l

        p = ROOT.TLatex()
        p.SetNDC()
        p.SetTextFont(42)
        p.SetTextSize(textSize) # added
        p.SetTextColor(self.color)
        self.p = p

        self.l.DrawLatex(self.x, self.y, "ATLAS")
        if self.text:
            self.p.DrawLatex(self.x+self.delx, self.y, self.text)

class LumiLabel(object):

    "Label showing the luminosity"

    def __init__(self, lumi=1., unit="fb^{-1}", scale=1., xOffset=0., yOffset=0.):
        self.lumi = lumi
        self.unit = unit
        self.scale = scale
        self.xOffset = xOffset
        self.yOffset = yOffset
        self.zIndex = 2

    def draw(self, *args, **kwargs):
        l = ROOT.TLatex()
        l.SetNDC()
        l.SetTextFont(gst.customTextFont)
        l.SetTextSize(gst.customTextSize)
        l.SetTextColor(1)
        self.l = l
        x = gst.lumiLabelX + self.xOffset
        y = scaleYPosTopMargin(gst.lumiLabelY) + self.yOffset
        self.l.DrawLatex(x, y, "#int L dt = %.1f %s" % (self.lumi, self.unit))


class CMELabel(object):

    "Label showing the center of mass energy"

    def __init__(self, cme=13, unit="TeV", scale=1., xOffset=0., yOffset=0.):
        self.cme = cme
        self.unit = unit
        self.scale = scale
        self.xOffset = xOffset
        self.yOffset = yOffset
        self.zIndex = 2

    def draw(self, *args, **kwargs):
        l = ROOT.TLatex()
        l.SetNDC()
        l.SetTextFont(43)
        l.SetTextSize(gst.CMELabelTextSize)
        l.SetTextColor(1)
        self.l = l
        x = gst.CMELabelX + self.xOffset
        y = scaleYPosTopMargin(gst.CMELabelY) + self.yOffset
        self.l.DrawLatex(x, y, "#sqrt{s} = %s %s" % (self.cme, self.unit))
