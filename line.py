import ROOT

from .commonHelpers.logger import logger
logger = logger.getChild(__name__)

from . import globalStyle as gst

class Line(ROOT.TLine):
    def __init__(self, *args, **kwargs):
        self.zIndex = 2
        self.lineStyle = kwargs.pop("lineStyle", gst.TLineStyle)
        self.lineWidth = kwargs.pop("lineWidth", gst.TLineWidth)
        self.lineColor = kwargs.pop("lineColor", gst.TLineColor)
        if kwargs:
            raise Exception("Got unexpected kwargs: {}".format(kwargs))
        super(Line, self).__init__(*args)

    def draw(self, **kwargs):
        self.SetLineStyle(self.lineStyle)
        self.SetLineWidth(self.lineWidth)
        self.SetLineColor(self.lineColor)
        self.Draw()
