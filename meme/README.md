# MEME - Memoize Even Mutable Entities

# TLDR:

Decorate a function using

```
import meme

@meme.cache()
def myCoolFunctionThatReturnsAlwaysTheSameWhenCalledWithSameArguments(*args, **kwargs):
    <my cool code>
```

This will create a cache that is stored persistently. If the function
is called again (also when running the program again later) it won't
be executed again, but instead the cached value is directly retrieved.

# README:

This tool is heavily inspired by similiar tools from Alexander Mann
and Balthasar Schachtner.

The design choices are:

* Focus on optimising the workflow when executing a program multiple
  times (not for calling functions multiple times during one execution)

* Only accept functions where all arguments (and keyword arguments)
  are "hashable" - what means in this context int, float and
  str. However, one can specify arguments that should be ignored when
  creating the hash.

* Provide an option to use json serialisation to be able to
  use (also nested) dictionaries and lists as arguments

* The user has to make sure a function (with same arguments) is unique

* Keep the cache in memory - load once with the first execution of a
  function and save at termination of the program.

* Keep one cache file per function

See the unit tests in `test.py` for some example use cases

To run them all:

```
python -m meme.test
```

# TODO:

* Introduce a locking mechanism to allow multiple executions of a
  program that write to the same cache file

* Add option for setting the cache directory as a relative path to the
  module file

* Develop some cleanup meachanisms

