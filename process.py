import ROOT
from .commonHelpers.logger import logger
from .commonHelpers.options import setAttributes

class Process:


    defaults = dict(
        style = "background",
        color = None,
        lineStyle =  None,
        lineWidth =  None,
        fillStyle = None,
        markerStyle = None,
        customLabel =  None,
        cut =  None,
        norm =  False,
        normToProcess = None,
        scale =  None,
        multiRatio =  False,
        legendTitle = None,
        drawString = "",
        varexp = None,
        ratioDenominatorProcess = None,
        stackOnTop = False,
    )
    setAttributes = setAttributes


    _doc_parameters = """
        Parameters to be used for :py:meth:`~MPF.plotStore.PlotStore.registerHist` for histograms created from the process:

        :param style: (default: "background")
        :param color: (default: None)
        :param lineStyle: (default: None)
        :param lineWidth: (default: None)
        :param fillStyle: (default: None)
        :param markerStyle: (default: None)
        :param drawString: (default: None)
        :param legendTitle: (default: None)
        :param ratioDenominatorProcess: (default: None)
        :param stackOnTop: (default: False)

        The other parameters:

        :param cut: cut/weight expression to be used only for this process
        :param norm: normalise the resulting histograms to unity?
        :param scale: scale resulting histogram by this factor
        :param varexp: use this varexp for this process **instead** of the one used for all the other processes
        :param normToProcess: normalise the histogram to the same
                              integral as the given process (by name) before plotting (only
                              used for hists of style "systematic" in :py:class:`~MPF.treePlotter.TreePlotter`)
    """


    def __init__(self, name, **kwargs):
        """
        Create a process to be used with
        :py:meth:`~MPF.processProjector.ProcessProjector`

        {parameters}

        """

        self.trees = []
        self.name = name
        self.hist = None
        self.bins = None
        self.yieldsDict = None
        self.setAttributes(self.defaults, **kwargs)
        logger.debug("Initialising process {}".format(self.name))
        logger.debug("Cut: {}".format(self.cut))

    __init__.__doc__ = __init__.__doc__.format(parameters=_doc_parameters)


    def addTree(self, filename, treename):
        self.trees.append((filename, treename))


    def setOptions(self, **kwargs):
        self.setAttributes(self.defaults, **kwargs)

    def rawEvents(self):
        from .histProjector import HistProjector
        HP = HistProjector()
        nEvts = 0
        for path, treeName in self.trees:
            cut = self.cut if self.cut is not None else "1"
            nEvts += HP.getYieldPath(treeName, cut, path)[0]
        return nEvts

    def __repr__(self):
        out = "<Process {}: ".format(self.name)
        out += "attributes: "
        for attribute in self.defaults:
            out += "{}={}, ".format(attribute, getattr(self, attribute))
        out += "hist: {} ".format(self.hist)
        out += ">"
        return out

