#!/usr/bin/env/python
"""
Overlay multiple histograms (added with style "signal") and plot
the ratio to the first one in the bottom pad.

Example
--------

.. literalinclude:: /../examples/signalRatioPlot.py

.. image::  images/signalRatioPlot.png
   :width: 600px

"""

from .plotStore import PlotStore

class SignalRatioPlot(PlotStore):

    """
    Overwrites the defaults for the following :py:meth:`~MPF.plotStore.PlotStore` parameters:

    :param ratioUp: default: 2.
    :param ratioDown: default: 0.
    :param ratioMode: default: "pois"
    :param ignoreNumErrors: default: False
    :param ignoreDenErrors: default: False

    For further options see :py:meth:`~MPF.plotStore.PlotStore`
    """

    def __init__(self, **kwargs):
        super(SignalRatioPlot, self).__init__(splitting='ratio',
                                              ratioUp=kwargs.pop("ratioUp", 2.),
                                              ratioDown=kwargs.pop("ratioDown", 0.),
                                              ratioMode=kwargs.pop("ratioMode", "hist"),
                                              ignoreNumErrors=kwargs.pop("ignoreNumErrors", False),
                                              ignoreDenErrors=kwargs.pop("ignoreDenErrors", False),
                                              **kwargs)

    def saveAs(self, path, **kwargs):
        self.buildMainPad(**kwargs)

        bottomPad = self.canvas.pads['bottom']
        self.addSignalRatios(bottomPad)
        bottomPad.yTitle = "Ratio"

        return super(SignalRatioPlot, self).saveAs(path, **kwargs)
