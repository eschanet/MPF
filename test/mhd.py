import unittest

import os
import shutil
from array import array

import ROOT

from .. import meme

from .. import histProjector
from .. import multiHistDrawer
from ..histProjector import HistProjector
from .. import pyrootHelpers as PH
from .. import IOHelpers as IO
from ..examples.exampleHelpers import createExampleTrees

from .helpers.floatTester import FloatTester

from ..commonHelpers.logger import logger
logger = logger.getChild(__name__)

class Test(FloatTester):

    testTreePath = "/tmp/testTreeMPF.root"

    @classmethod
    def setUpClass(cls):
        # multiHistDrawer.skipCleanup = True # this can help for debugging
        # meme should be deactivated for these tests
        cls.memeWasActivated = not meme.getOption("deactivated")
        logger.info("Deactivating meme")
        meme.setOptions(deactivated=True)
        if not os.path.exists(cls.testTreePath):
            logger.info("creating example tree {}".format(cls.testTreePath))
            createExampleTrees(cls.testTreePath)


    @classmethod
    def tearDownClass(cls):
        if not cls.memeWasActivated:
            meme.setOptions(deactivated=False)


    def test_fixed_bins(self):
        hp = HistProjector()
        histargs = ("w1", "met>200", self.testTreePath)
        histkwargs = dict(varexp="ht",
                          weight="w",
                          nbins=1000,
                          xmin=0,
                          xmax=5000)
        hist1 = hp.getTH1Path(*histargs, **histkwargs)
        hp.registerTH1Path(*histargs, **histkwargs)
        hp.fillHists()
        hist2 = hp.getTH1Path(*histargs, **histkwargs)
        self.assertEqualHists(hist1, hist2)


    def test_same_xTitle(self):
        hp = HistProjector()
        histargs = ("w1", "met>200", self.testTreePath)
        histkwargs = dict(varexp="ht",
                          weight="w",
                          nbins=1000,
                          xmin=0,
                          xmax=5000)
        hist1 = hp.getTH1Path(*histargs, **histkwargs)
        hp.registerTH1Path(*histargs, **histkwargs)
        hp.fillHists()
        hist2 = hp.getTH1Path(*histargs, **histkwargs)
        self.assertEqual(hist1.GetXaxis().GetTitle(), hist2.GetXaxis().GetTitle())


    def test_zero_weights(self):
        hp = HistProjector()
        histargs = ("w1", "met>200", self.testTreePath)
        histkwargs = dict(varexp="ht",
                          weight="(1-w)>0 ? (1-w) : 0",
                          nbins=100,
                          xmin=0,
                          xmax=5000)
        hist1 = hp.getTH1Path(*histargs, **histkwargs)
        hp.registerTH1Path(*histargs, **histkwargs)
        hp.fillHists()
        hist2 = hp.getTH1Path(*histargs, **histkwargs)
        self.assertEqualHists(hist1, hist2)


    def test_yield(self):
        hp = HistProjector()
        histargs = ("w1", "met>150", self.testTreePath)
        histkwargs = dict(weight="sqrt(abs(1-w*10))*max(sin(w), cos(w))")
        n1, y1, dy1 = hp.getYieldPath(*histargs, **histkwargs)
        hp.registerYieldPath(*histargs, **histkwargs)
        hp.fillHists()
        logger.debug("Filled hists")
        n2, y2, dy2 = hp.getYieldPath(*histargs, **histkwargs)
        self.assertEqual(n1, n2)
        self.assertClose(y1, y2)
        self.assertClose(dy1, dy2)


    def test_varbins(self):
        hp = HistProjector()
        histargs = ("w1", "met>125", self.testTreePath)
        histkwargs = dict(weight="w", varexp="ht")
        varBinKwargs = dict(binLowEdges=[i*100 for i in range(10+1)])
        fixedBinKwargs = dict(xmin=0, xmax=1000, nbins=10)
        hist1 = hp.getTH1Path(*histargs, **dict(fixedBinKwargs, **histkwargs))
        hist2 = hp.getTH1Path(*histargs, **dict(varBinKwargs, **histkwargs))
        hp.registerTH1Path(*histargs, **dict(fixedBinKwargs, **histkwargs))
        hp.registerTH1Path(*histargs, **dict(varBinKwargs, **histkwargs))
        hp.fillHists()
        hist3 = hp.getTH1Path(*histargs, **dict(fixedBinKwargs, **histkwargs))
        hist4 = hp.getTH1Path(*histargs, **dict(varBinKwargs, **histkwargs))
        self.assertEqualHists(hist1, hist2)
        self.assertEqualHists(hist2, hist3)
        self.assertEqualHists(hist3, hist4)


    def test_yieldsDict(self):
        hp = HistProjector()
        cutsDict = {
            "region1" : "met>100&&ht>1000",
            "region2" : "ptl>100",
            "region3" : "(ptl>100)*1000",
        }
        args = ("w1", cutsDict, self.testTreePath)
        kwargs = dict(weight="w")
        hist1 = hp.getYieldsHist(*args, **kwargs)
        hp.registerYieldsHist(*args, **kwargs)
        hp.fillHists()
        hist2 = hp.getYieldsHist(*args, **kwargs)
        self.assertEqualHists(hist1, hist2)


    def test_alias(self):
        hp = HistProjector()
        funexpr1 = "(ptl+ptj)/met*ht*njet"
        funexpr2 = "(1-50*(1-w))"
        binning = dict(xmin=0, xmax=1000, nbins=100)
        hist1 = hp.getTH1Path("w1", "met>200", self.testTreePath,
                              weight=funexpr2,
                              varexp=funexpr1,
                              **binning)
        hp.setAlias("funexpr1", funexpr1)
        hp.setAlias("funexpr2", funexpr2)
        hist2 = hp.getTH1Path("w1", "met>200", self.testTreePath,
                              weight="funexpr2",
                              varexp="funexpr1",
                              **binning)
        self.assertEqualHists(hist1, hist2)


    def test_composite_varexp(self):
        hp = HistProjector()
        args = ("w1", "1", self.testTreePath)
        kwargs = dict(xmin=0, xmax=20, nbins=20, varexp="(met/sqrt(ht))")
        hist1 = hp.getTH1Path(*args, **kwargs)
        hp.registerTH1Path(*args, **kwargs)
        hp.fillHists()
        hist2 = hp.getTH1Path(*args, **kwargs)
        self.assertEqualHists(hist1, hist2)


    def test_paths_treeNames_onetree(self):
        hp = HistProjector()
        args = ("w1", "1", self.testTreePath)
        kwargs = dict(xmin=0, xmax=20, nbins=20, varexp="(met/sqrt(ht))")
        hist1 = hp.getTH1Path(*args, **kwargs)
        hist2 = hp.getTH1PathTrees([(self.testTreePath, "w1")], "1", **kwargs)
        self.assertEqualHists(hist1, hist2)


    def test_compile(self):
        hp = HistProjector()
        args = ("w1", "1", self.testTreePath)
        kwargs = dict(xmin=0, xmax=20, nbins=20, varexp="(met/sqrt(ht))")
        hp.registerTH1Path(*args, **kwargs)
        hp.fillHists(compile=False)
        hist1 = hp.getTH1Path(*args, **kwargs)
        hp.fillHists(compile=True)
        hist2 = hp.getTH1Path(*args, **kwargs)
        self.assertEqualHists(hist1, hist2)


    def test_simple_vec_branch(self):
        hp = HistProjector()
        args = ("w1", "1", self.testTreePath)
        kwargs = dict(xmin=0, xmax=1000, nbins=20, varexp="jets_pt[0]+jets_pt[1]")
        hist1 = hp.getTH1Path(*args, **kwargs)
        hp.registerTH1Path(*args, **kwargs)
        hp.fillHists()
        hist2 = hp.getTH1Path(*args, **kwargs)
        self.assertEqualHists(hist1, hist2)


if __name__ == "__main__":
    unittest.main()
