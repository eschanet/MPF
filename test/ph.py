import unittest
import os

from .. import pyrootHelpers as PH
from .. import IOHelpers as IO
from ..examples.exampleHelpers import createExampleTrees

from ..commonHelpers.logger import logger
logger = logger.getChild(__name__)

class Test(unittest.TestCase):

    testTreePath = "testTreeMPF.root"

    @classmethod
    def setUpClass(cls):
        if not os.path.exists(cls.testTreePath):
            logger.info("creating example tree {}".format(cls.testTreePath))
            createExampleTrees(cls.testTreePath)

    def test_printTreeNames(self):
        print(PH.getTreeNamesFromFile(self.testTreePath))

