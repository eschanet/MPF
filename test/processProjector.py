import os

from .helpers.floatTester import FloatTester

from ..processProjector import ProcessProjector
from ..histProjector import HistProjector
from ..examples.exampleHelpers import createExampleTrees

from ..commonHelpers.logger import logger
logger = logger.getChild(__name__)

class TestYieldsDict(FloatTester):


    testTreePath = "/tmp/testTreeMPF.root"


    @classmethod
    def setUpClass(cls):
        if not os.path.exists(cls.testTreePath):
            logger.info("creating example tree {}".format(cls.testTreePath))
            createExampleTrees(cls.testTreePath)


    def test_oneYield(self):
        """Test if one yield is propagated correctly to the yields dict"""
        pp = ProcessProjector(cutsDict=dict(test="met>200"))
        pp.addProcessTree("w1", self.testTreePath, "w1")
        pp.fillYieldsDicts(pp.getOpt())
        n, y, dy = HistProjector().getYieldPath("w1", "met>200", self.testTreePath)
        n2, y2, dy2 = pp.getProcess("w1").yieldsDict["test"]
        self.assertEqual(n, n2)
        self.assertClose(y, y2)
        self.assertClose(dy, dy2)


    def test_cmp_hist_yield(self):
        """Test if projecting 2 bins in a cutsDict gives consistent results to
        projecting one histogram with 2 bins"""
        pp = ProcessProjector(cutsDict=dict(bin1="met>200&&met<=300", bin2="met>300&&met<=400"))
        pp.addProcessTree("w1", self.testTreePath, "w1")
        pp.fillYieldsDicts(pp.getOpt())
        hist1 = pp.getProcess("w1").hist
        pp2 = ProcessProjector(varexp="met", nbins=2, xmin=200, xmax=400, cut="met>200&&met<=400")
        pp2.addProcessTree("w1", self.testTreePath, "w1")
        pp2.fillHists(pp2.getOpt())
        hist2 = pp2.getProcess("w1").hist
        self.assertEqualHists(hist1, hist2)
