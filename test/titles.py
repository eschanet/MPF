import unittest
import itertools
import os
import shutil

from ..plot import Plot
from ..examples.exampleHelpers import histogramFromList
from .. import pyrootHelpers as PH
from .helpers.pdfCollectionTest import PDFCollectionTest

from ..commonHelpers.logger import logger
logger = logger.getChild(__name__)

class TestHist(PDFCollectionTest):

    debugText = ""

    def setUp(self):
        fallingDistribution = [(float(100-i%100), float(i)) for i in range(500)]
        lessFallingDistribution = [(float(100-i%100), float(i*0.3+100)) for i in range(500)]
        risingDistribution = [(float(i%100), float(i)) for i in range(500)]
        for l in [fallingDistribution, lessFallingDistribution, risingDistribution]:
            l.append((0, 0))
            l.append((100, 0))
        self.histo1 = histogramFromList(fallingDistribution, 10)
        self.histo2 = histogramFromList(risingDistribution, 10)
        self.histo3 = histogramFromList(lessFallingDistribution, 10)
        self.labelled_histo1 = histogramFromList(fallingDistribution, 10)
        for i in range(10):
            self.labelled_histo1.GetXaxis().SetBinLabel(i+1,"({})".format(i))
        self.plot = Plot()


    def tearDown(self):
        self.plot.debugText = [self.debugText]
        self.pdf = self.plot.saveAs(next(self.pdfNames))
        super(TestHist, self).tearDown()


    @classmethod
    def setUpClass(cls):
        cls.mergedName = "titleTests.pdf"
        super(TestHist, cls).setUpClass()


    def test_root_xTitle(self):
        self.debugText = "xTitle from ROOT hist"
        self.histo1.GetXaxis().SetTitle("correct title")
        self.plot.registerHist(self.histo1, style="background")


    def test_hm_xTitle(self):
        # this one is probably not really important
        self.debugText = "xTitle from hm hist"
        self.histo1.GetXaxis().SetTitle("wrong title")
        self.plot.registerHist(self.histo1, style="background")
        self.plot.objects[0].xTitle = "correct title"


    def test_plot_xTitle(self):
        # note: plot is not overwriting hm title since this is
        # happening when the hist is registered
        self.debugText = "xTitle from plot"
        self.plot = Plot(xTitle="correct title")
        self.plot.registerHist(self.histo1, style="background")


    def test_pad_xTitle(self):
        self.debugText = "xTitle from pad"
        self.plot = Plot(xTitle="wrong title")
        self.plot.registerHist(self.histo1, style="background")
        self.plot.canvas.pads["main"].xTitle = "correct title"


    def test_pad_yTitle(self):
        self.debugText = "yTitle from pad"
        self.plot = Plot()
        self.plot.registerHist(self.histo1, style="background")
        self.plot.canvas.pads["main"].yTitle = "custom yTitle"


    def test_unit(self):
        self.debugText = "xTitle with unit from plot"
        self.plot = Plot(xTitle="correct title", unit="correct units")
        self.plot.registerHist(self.histo1, style="background")


    def test_signal(self):
        self.debugText = "xTitle with unit from plot (signal style)"
        self.plot = Plot(xTitle="correct title", unit="correct units")
        self.plot.registerHist(self.histo1, style="signal")


    def test_data(self):
        self.debugText = "xTitle with unit from plot (data style)"
        self.plot = Plot(xTitle="correct title", unit="correct units")
        self.plot.registerHist(self.histo1, style="data")

    def test_plot_xAxisLabelsOption_default(self):
        self.debugText = "default labels"
        self.plot = Plot()
        self.plot.registerHist(self.labelled_histo1, style="background")

    def test_plot_xAxisLabelsOption_vertical(self):
        self.debugText = "vertical labels"
        self.plot = Plot(xAxisLabelsOption="v")
        self.plot.registerHist(self.labelled_histo1, style="background")
