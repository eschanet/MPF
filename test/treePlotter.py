import os
import math
import collections

from ..meme import meme

from ..treePlotter import TreePlotter
from ..process import Process
from ..pyrootHelpers import TangoColors as tcol
from .. import pyrootHelpers as PH
from ..examples.exampleHelpers import createExampleTrees
from .. import globalStyle as gst

from .helpers.pdfCollectionTest import PDFCollectionTest

from MPF.commonHelpers.logger import logger
logger = logger.getChild(__name__)

pdfs = []

def tearDownModule():
    """Merge the pdfs of all tests here"""
    if len(pdfs) > 0:
        try:
            PH.pdfUniteOrMove(pdfs, "treeHists.pdf")
        except IOError:
            logger.error("Couldn't merge pdfs: {}".format(pdfs))


class BaseTest(PDFCollectionTest):

    """Base class for all treePlotter tests. Creates the test trees on
    setUpClass and appends the merged pdfs to the module pdfs (which will
    finally also be merged in the end)"""

    @classmethod
    def setUpClass(cls):

        cls.memeWasActivated = not meme.getOption("deactivated")
        logger.info("Deactivating meme")
        meme.setOptions(deactivated=True)

        super(BaseTest, cls).setUpClass()
        cls.mergedName = next(cls.pdfNames)

        cls.testTreePath = "/tmp/testTreeMPF.root"
        if not os.path.exists(cls.testTreePath):
            logger.info("creating example tree {}".format(cls.testTreePath))
            createExampleTrees(cls.testTreePath)


    @classmethod
    def tearDownClass(cls):
        if cls.memeWasActivated:
            meme.setOptions(deactivated=False)
        super(BaseTest, cls).tearDownClass()
        if cls.imageFormat == "pdf":
            pdfs.append(cls.mergedName)


def setUpBkgSig(self):

    # Default Options can already be set when the plotter is initialized
    self.tp = TreePlotter(cut="ht>1000", varexp="met", xTitle="E_{T}^{miss}", unit="GeV")

    # Processes and their corresponding trees can be added via addProcessTree
    self.tp.addProcessTree("bkg1", self.testTreePath, "w1", cut="(1-50*(1-w))", style="background", color=tcol.blue2)
    self.tp.addProcessTree("bkg2", self.testTreePath, "w2", style="background", color=tcol.green2)
    self.tp.addProcessTree("sig", self.testTreePath, "w4", style="signal", color=tcol.red2)

class TestBkgSig(BaseTest):

    def setUp(self):
        setUpBkgSig(self)


    def test_no_data(self):
        self.pdf = self.tp.plot(next(self.pdfNames), xmin=0., xmax=1000, nbins=100, debugText=["test_no_Data"])

    def test_no_data_overflow(self):
        self.pdf = self.tp.plot(next(self.pdfNames), xmin=0., xmax=1000, nbins=100, debugText=[self._testMethodName], addOverflowToLastBin=True, logy=True)

    def test_no_data_overflow_with_note(self):
        self.pdf = self.tp.plot(next(self.pdfNames), xmin=0., xmax=1000, nbins=100, debugText=[self._testMethodName], addOverflowToLastBin=True, addOverflowBinNote=True, logy=True)

    def test_no_data_norm(self):
        self.tp.getProcess("bkg1").norm = True
        self.tp.getProcess("bkg2").norm = True
        self.tp.getProcess("sig").scale = 0.0001
        with gst.useOptions(legendShowScaleFactors=False):
            self.pdf = self.tp.plot(next(self.pdfNames), xmin=0., xmax=1000, nbins=100, debugText=[self._testMethodName],
                                    legendOptions=dict(addEventCount=True))

    def test_no_data_norm_mhd(self):
        self.tp.getProcess("bkg1").norm = True
        self.tp.getProcess("bkg2").norm = True
        self.tp.getProcess("sig").scale = 0.0001
        self.pdf = self.tp.registerPlot(next(self.pdfNames), xmin=0., xmax=1000, nbins=100, debugText=[self._testMethodName],
                                        legendOptions=dict(addEventCount=True))
        self.tp.plotAll(useMultiHistDraw=True, compile=False)

    def test_no_data_extra_opts(self):
        self.pdf = self.tp.plot(next(self.pdfNames), cut="met>200", varexp="ht", xmin=0., xmax=5000, nbins=100, xTitle="H_{T}",
                                debugText=["test_no_data_extra_opts"])

    def test_legend_opts(self):
        # Options for the legend can be passed as a dictionary
        self.pdf = self.tp.plot(next(self.pdfNames), cut="met>200", varexp="ht", xmin=0., xmax=5000, nbins=100, xTitle="H_{T}",
                                legendOptions=dict(xOffset=-0.1, yOffset=-0.1, scaleLegend=2.),
                                debugText=["test_legend_opts"])

class TestDataMC(BaseTest):

    def setUp(self):
        setUpBkgSig(self)
        # If multiple trees/files correspond to one process then you can
        # explicitely create the Process object and add it to the plotter
        data = Process("data", style="data")
        data.addTree(self.testTreePath, "w1")
        data.addTree(self.testTreePath, "w2")
        self.tp.addProcess(data)
        # Defaults can also be overwritten at any time
        self.tp.setDefaults(xmin=0., xmax=3000, nbins=100, plotType="DataMCRatioPlot", ratioUp=1.25, ratioDown=0.75)
        self.testImages = []

    def test_dataMC(self):
        self.pdf = self.tp.plot(next(self.pdfNames), debugText=["test_dataMC"])

    def test_dataMC_scaleBkg1_08(self):
        self.tp.getProcess("bkg1").scale=0.8
        self.pdf = self.tp.plot(next(self.pdfNames), debugText=[self._testMethodName])

    def test_dataMC_eventCounts_legend(self):
        self.pdf = self.tp.plot(next(self.pdfNames),
                                legendOptions=dict(addEventCount=True),
                                debugText=["test_dataMC_eventCounts_legend"])

    def test_dataMC_eventCountsGen_legend(self):
        self.pdf = self.tp.plot(next(self.pdfNames),
                                legendOptions=dict(addEventCount=True, eventCountGen=True),
                                debugText=["test_dataMC_eventCountsGen_legend"])

    def test_dataMC_eventCountsCutflow_legend(self):
        self.pdf = self.tp.plot(next(self.pdfNames),
                                nbins=4,
                                logy=True,
                                legendOptions=dict(addEventCount=True, eventCountCutflow=True),
                                debugText=["test_dataMC_eventCountsCutflow_legend"])

    def test_dataMC_log(self):
        self.pdf = self.tp.plot(next(self.pdfNames), logy=True, ratioUp=2.25, ratioDown=0.25, debugText=["test_dataMC_log"])

    def test_dataMC_bgCont(self):
        self.pdf = self.tp.plot(next(self.pdfNames), logy=True, ratioUp=2.25, ratioDown=0.25, plotType="BGContributionPlot", debugText=["test_dataMC_bgCont"])

    def test_multiHistDraw(self):
        self.pdf = None
        # Try a few plots with multiHistDraw
        self.tp.setDefaults(varexp="ht", xTitle="H_{T}", xmin=0., xmax=3000, nbins=100, cut="1", plotType="DataMCRatioPlot", ratioUp=1.25, ratioDown=0.75)
        for metcut in range(200, 300, 10):
            self.testImages.append(self.tp.registerPlot(next(self.pdfNames), cut="met>{}".format(metcut), debugText=["test_multiHistDraw_met{}".format(metcut)]))
        # for very (very very) many histograms "compile=True" might improve the performance
        self.tp.plotAll(useMultiHistDraw=True, compile=False)

    def test_custom_bin_labels(self):
        self.tp.setDefaults(varexp="ht", xTitle="H_{T}", xmin=0., xmax=3000, nbins=100, cut="1", plotType="DataMCRatioPlot", ratioUp=1.25, ratioDown=0.75)
        self.pdf = self.tp.plot(next(self.pdfNames),
                                xmin=0, xmax=3, nbins=3,
                                binLabels=["One", "Two", "Three"], xTitle='', unit=None,
                                ratioUp=2.25, ratioDown=0.25,
                                debugText=["test_custom_bin_labels"])

    def test_corr_sys(self):
        self.tp.addSysTreeToProcess("bkg1", "sys1", self.testTreePath, "w1", cut="1+met*0.001")
        self.tp.addSysTreeToProcess("bkg2", "sys1", self.testTreePath, "w2", cut="1+met*0.001")
        self.pdf = self.tp.plot(next(self.pdfNames), logy=True, ratioUp=2.25, ratioDown=0.25, plotType="BGContributionPlot",
                                debugText=["test_corr_sys"])

    def test_uncorr_sys(self):
        self.tp.addSysTreeToProcess("bkg1", "sys1", self.testTreePath, "w1", cut="1+met*0.001")
        self.tp.addSysTreeToProcess("bkg2", "sys2", self.testTreePath, "w2", cut="1+met*0.001")
        self.pdf = self.tp.plot(next(self.pdfNames), logy=True, ratioUp=2.25, ratioDown=0.25, plotType="BGContributionPlot",
                                debugText=["test_uncorr_sys"])

    def test_uncorr_sys_w_err_legend(self):
        self.tp.addSysTreeToProcess("bkg1", "sys1", self.testTreePath, "w1", cut="1+met*0.001")
        self.tp.addSysTreeToProcess("bkg2", "sys2", self.testTreePath, "w2", cut="1+met*0.001")
        self.pdf = self.tp.plot(next(self.pdfNames), logy=True, ratioUp=2.25, ratioDown=0.25, plotType="BGContributionPlot",
                                drawTotalBGErrorLegend=True,
                                debugText=["test_uncorr_sys_w_err_legend"])

    def test_noSys_w_err_legend(self):
        self.pdf = self.tp.plot(next(self.pdfNames), logy=True, ratioUp=2.25, ratioDown=0.25, plotType="BGContributionPlot",
                                drawTotalBGErrorLegend=True,
                                debugText=["test_NoSys_w_err_legend"])


class TestMultiSignal(BaseTest):

    def setUp(self):
        # To change the process composition, create a new plotter
        tp2 = TreePlotter(cut="1", varexp="mt", xTitle="m_{T}", unit="GeV")
        tp2.addProcessTree("bkg1", self.testTreePath, "w1", cut="(1-50*(1-w))", style="background", color=tcol.blue2)
        signalOpts = dict(cut="0.1", style="signal", lineWidth=5)
        tp2.addProcessTree("sig1", self.testTreePath, "w2", color="kPink", **signalOpts)
        tp2.addProcessTree("sig2", self.testTreePath, "w3", color="kPink+1", **signalOpts)
        tp2.addProcessTree("sig3", self.testTreePath, "w4", color="kPink+2", **signalOpts)
        self.tp = tp2


    def test_significance_scan(self):
        self.pdf = self.tp.plot(next(self.pdfNames), logy=True, weight="0.001", xmin=0., xmax=2000, nbins=20,
                                plotType="SignificanceScanPlot", debugText=["test_significance_scan"])

    def test_significance_scan_PoissonAsimov(self):
        self.pdf = self.tp.plot(next(self.pdfNames), logy=True, weight="0.001", xmin=0., xmax=2000, nbins=20, significanceMode="PoissonAsimov",
                                plotType="SignificanceScanPlot", debugText=["test_significance_scan_PoissonAsimov"])

    def test_significance_scan_custom(self):
        self.pdf = self.tp.plot(next(self.pdfNames), logy=True, weight="0.001", xmin=0., xmax=2000, nbins=20,
                                plotType="SignificanceScanPlot", significanceFunction=lambda s, b, db : s/math.sqrt(b),
                                debugText=["test_significance_scan_custom (s/sqrt(b))"])


class TestMultiData(BaseTest):

    def setUp(self):
        tp = TreePlotter()
        tp.addProcessTree("data 1", self.testTreePath, "w1", style="data")
        tp.addProcessTree("data 2", self.testTreePath, "w3", style="data")
        self.tp = tp

    def test_multi_data_tree(self):
        self.pdf = self.tp.plot(next(self.pdfNames), varexp="m", xmin=0, xmax=500, nbins=100)

    def test_multi_data_tree_customMarker(self):
        self.tp.getProcess("data 2").markerStyle = 24
        self.tp.getProcess("data 2").color = "kRed"
        self.pdf = self.tp.plot(next(self.pdfNames), varexp="m", xmin=0, xmax=500, nbins=100)


class TestStackSignal(BaseTest):

    def setUp(self):
        # To change the process composition, create a new plotter
        tp2 = TreePlotter(cut="1", varexp="mt", xTitle="m_{T}", unit="GeV")
        tp2.addProcessTree("bkg1", self.testTreePath, "w1", cut="(1-50*(1-w))", style="background", color=tcol.blue2)
        tp2.addProcessTree("sig3", self.testTreePath, "w4", color="kPink+2",
                           cut="0.1", style="signal", lineWidth=5, stackOnTop=True, fillStyle=1001)
        self.tp = tp2

    def test_stack_signal(self):
        self.pdf = self.tp.plot(next(self.pdfNames), logy=True, weight="0.001", xmin=0., xmax=2000, nbins=20,
                                plotType="SignificanceScanPlot", debugText=["test_stack_signal"])


class TestSignalRatio(BaseTest):

    def setUp(self):
        tp = TreePlotter(varexp="met", xmin=0., xmax=1500, nbins=50,
                         xTitle="E_{T}^{miss}", unit="GeV",
                         ratioUp=2.25, ratioDown=0.,
                         plotType="SignalRatioPlot", drawRatioLine=False)
        commonOpts = dict(norm=True, style="signal", lineStyle=1, lineWidth=3)
        tp.addProcessTree("HT > 1000", self.testTreePath, "w1", cut="ht>1000", color=tcol.blue2, **commonOpts)
        tp.addProcessTree("HT > 1200", self.testTreePath, "w1", cut="ht>1200", color=tcol.red2, **commonOpts)
        tp.addProcessTree("HT > 1400", self.testTreePath, "w1", cut="ht>1400", color=tcol.green2, **commonOpts)
        self.tp = tp


    def test_signal_ratio_hist(self):
        self.pdf = self.tp.plot(next(self.pdfNames), ratioMode="hist", debugText=["test_signal_ratio_hist"])

    def test_signal_ratio_pois(self):
        self.pdf = self.tp.plot(next(self.pdfNames), ratioMode="pois", debugText=["test_signal_ratio_pois"])


class TestEfficiency(BaseTest):

    def setUp(self):
        tp = TreePlotter(varexp="met", xmin=0., xmax=1500, nbins=50,
                         weight="w",
                         xTitle="E_{T}^{miss}",
                         unit="GeV",
                         yMax=1.4,
                         plotType="EfficiencyPlot")
        commonOpts = dict(style="signal")
        tp.addProcessTree("Base", self.testTreePath, "w1", color=tcol.blue2, **commonOpts)
        tp.addProcessTree("HT > 1000", self.testTreePath, "w1", cut="ht>1000", color=tcol.blue2, **commonOpts)
        tp.addProcessTree("HT > 1200", self.testTreePath, "w1", cut="ht>1200", color=tcol.red2, **commonOpts)
        tp.addProcessTree("HT > 1400", self.testTreePath, "w1", cut="ht>1400", color=tcol.green2, **commonOpts)
        self.tp = tp

    def test_efficiencies(self):
        self.pdf = self.tp.plot(next(self.pdfNames), debugText=["test_efficiencies"])

    def test_efficiencies_bayes(self):
        self.pdf = self.tp.plot(next(self.pdfNames), debugText=["test_efficiencies_bayes"], ratioMode="bayes")


class TestEfficiencyTargetDenominator(BaseTest):

    def setUp(self):
        tp = TreePlotter(varexp="met", xmin=0., xmax=1500, nbins=50,
                         xTitle="E_{T}^{miss}",
                         unit="GeV",
                         yMax=1.4,
                         plotType="EfficiencyPlot")
        commonOpts = dict(style="signal")
        tp.addProcessTree("Base1", self.testTreePath, "w1", color=tcol.blue2, **commonOpts)
        tp.addProcessTree("Base2", self.testTreePath, "w1", cut="ht>2000", color=tcol.blue2, **commonOpts)
        tp.addProcessTree("Normal", self.testTreePath, "w1",
                          cut="(met*((1-w)*5+1))>400", color=tcol.blue2,
                          ratioDenominatorProcess="Base1", **commonOpts)
        tp.addProcessTree("HT > 2000",
                          self.testTreePath, "w1", cut="(met*((1-w)*5+1))>400&&ht>2000",
                          color=tcol.red2, ratioDenominatorProcess="Base2", **commonOpts)
        self.tp = tp

    def test_efficiencies_targetDenominator(self):
        self.pdf = self.tp.plot(next(self.pdfNames), debugText=["test_efficiencies_targetDenominator"])


class TestCutsDict(BaseTest):

    def setUp(self):
        tp = TreePlotter(plotType="BGContributionPlot", noData=True)
        tp.addProcessTree("bkg1", self.testTreePath, "w1", style="background", color=tcol.blue2)
        tp.addProcessTree("bkg2", self.testTreePath, "w2", style="background", color=tcol.green2)
        self.tp = tp


    def test_selections_per_bin(self):
        self.pdf = self.tp.plot(next(self.pdfNames),
                                cutsDict=collections.OrderedDict([
                                    ("met>200", "met>200"),
                                    ("met>300", "met>300"),
                                    ("ht>1000", "ht>1000"),
                                ]),
                                debugText=["test_selections_per_bin"])


    def test_selections_per_bin_scaleBkg1_08(self):
        self.tp.getProcess("bkg1").scale = 0.8
        self.pdf = self.tp.plot(next(self.pdfNames),
                                cutsDict=collections.OrderedDict([
                                    ("met>200", "met>200"),
                                    ("met>300", "met>300"),
                                    ("ht>1000", "ht>1000"),
                                ]),
                                debugText=[self._testMethodName])


class TestMultiVarexp(BaseTest):

    def setUp(self):
        tp = TreePlotter()
        tp.addProcessTree("ht", self.testTreePath, "w1", style="signal", color=tcol.blue2, varexp="ht")
        tp.addProcessTree("met", self.testTreePath, "w1", style="signal", color=tcol.green2, varexp="met")
        self.tp = tp
        self.testImages = []

    def test_overlay_2_varexp(self):
        self.pdf = self.tp.plot(next(self.pdfNames), nbins=100, xmin=0, xmax=1000, debugText=["test_overlay_2_varexp"])

    def test_overlay_2_varexp_register(self):
        self.pdf = None
        self.testImages.append(self.tp.registerPlot(next(self.pdfNames), nbins=100, xmin=0, xmax=1000, debugText=["test_overlay_2_varexp_register_ht100"], cut="ht>100"))
        self.testImages.append(self.tp.registerPlot(next(self.pdfNames), nbins=100, xmin=0, xmax=1000, debugText=["test_overlay_2_varexp_register_ht200"], cut="ht>200"))
        self.tp.plotAll(useMultiHistDraw=True, compile=False)
